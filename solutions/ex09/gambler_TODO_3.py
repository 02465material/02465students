        r = 1 if s + a == 100 else 0
        from collections import defaultdict
        WIN = (s+a, r)
        LOSS = (s-a, 0)
        return {WIN: self.p_heads, LOSS: 1-self.p_heads } if WIN != LOSS else {WIN: 1.}

        # return {(s + a, r): self.p_heads, (s - a, 0): 1 - self.p_heads} 