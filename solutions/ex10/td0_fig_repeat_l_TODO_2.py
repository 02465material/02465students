        
        for r in range(runs):
            errors = []
            V_current = np.copy(V_init)
            for i in range(episodes):
                errors.append(np.sqrt(np.sum(np.power(V_true - V_current, 2)) / 5.0))
                if method == 'TD':
                    temporal_difference(V_current, alpha=alpha)
                else:
                    monte_carlo(V_current, alpha=alpha)
            rms_errors += np.asarray(errors)
        rms_errors /= runs
        