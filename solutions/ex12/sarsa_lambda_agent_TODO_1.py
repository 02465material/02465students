        a_prime = self.pi_eps(sp) if not done else -1 
        delta = r + self.gamma * self.Q[sp][a_prime] - self.Q[s][a]
        self.e[s][self.a] += 1
        for s, Qs in self.Q.items():
            for a, q_sa in enumerate(Qs):
                self.Q[s][a] += self.alpha * delta * self.e[s][a]
                self.e[s][a] = self.gamma * self.lamb * self.e[s][a] 