        n, t = self.n, self.t
        if t == 0:  # We are in the initial state. Reset buffer.
            self.S[0], self.A[0] = s, a

        gamma = self.gamma
        self.S[(t + 1) % (n + 1)] = sp
        self.R[(t + 1) % (n + 1)] = r
        self.A[(t + 1) % (n + 1)] = eps_greedy(self.env, self.qhat, sp, self.w, self.epsilon) if not done else -1

        if done:
            T = t + 1
            tau_steps_to_train = range(t - n + 1, T)
        else:
            T = 1e10
            tau_steps_to_train = [t - n + 1]

        for tau in tau_steps_to_train:
            if tau >= 0:
                G = sum([self.gamma ** (i - tau - 1) * self.R[i%(n + 1)] for i in range(tau + 1, min(tau + n, T) + 1)])
                S_tau_n, A_tau_n = self.S[(tau + n) % (n + 1)], self.A[(tau + n) % (n + 1)]
                if tau + n < T:
                    G += gamma ** n * self.qhat.qhat(S_tau_n, A_tau_n, self.w) # Q[S_tau_n][A_tau_n]
                S_tau, A_tau = self.S[tau % (n + 1)], self.A[tau % (n + 1)]
                delta = (G - self.qhat.qhat(S_tau, A_tau, self.w))
                if n == 1:
                    delta_Sarsa = (r + self.gamma * self.qhat.qhat(sp, A_tau_n, self.w) - self.qhat.qhat(S_tau, A_tau, self.w))
                    if abs(delta - delta_Sarsa) > 1e-10:
                        raise Exception("n=1 agreement with Sarsa learning failed. You have at least one bug!")
                F = self.qhat.F(S_tau, A_tau)
                self.w[F] += self.alpha * delta

        self.t += 1
        if done:  # terminal state. Reset to t == 0.
            self.t = 0 