        delta = r - self.qhat.qhat(s,a,self.w)
        if done:
            a_prime = -1
        else:
            a_prime = eps_greedy(self.env, self.qhat, sp, self.w, self.epsilon)
            delta += self.gamma * self.qhat.qhat(sp, a_prime, self.w)

        F = self.qhat.F(s, a)
        self.w[F] += self.alpha * delta
        self.a = a_prime 