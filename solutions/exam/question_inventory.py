from irlc.exam.exam2023spring.inventory import InventoryDPModel
from irlc.exam.exam2023spring.dp import DP_stochastic
from irlc.ex02.dp_model import DPModel
import numpy as np

class InventoryDPModelB(InventoryDPModel):
    WB = "burglary"
    def __init__(self, N=3, c=0., p_burglary=0., prob_empty=False):
        self.c = c
        self.p_burglary = p_burglary
        self.prob_empty = prob_empty
        super().__init__(N=N)

    def g(self, x, u, w, k):  # Cost function g_k(x,u,w)
        if self.prob_empty:
            return 0
        if w == self.WB:
            return 0
        return u * self.c + np.abs(x + u - w)

    def f(self, x, u, w, k):  # Dynamics f_k(x,u,w)
        if w == self.WB:
            return 0
        return max(0, min(max(self.S(k)), x + u - w))

    def Pw(self, x, u, k):  # Distribution over random disturbances
        pw = {0: .1, 1: .3, 2: .6}
        if self.p_burglary is None:
            return pw
        else:
            return {**{w: (1 - self.p_burglary) * p for w, p in pw.items()}, **{self.WB: self.p_burglary}}

    def gN(self, x):
        if self.prob_empty:
            return -1 if x == 1 else 0
        else:
            return 0

def a_get_policy(N: int, c: float, x0 : int) -> int:
    model = InventoryDPModelB(N=N, c=c, p_burglary=0, prob_empty=False)
    J, pi = DP_stochastic(model)
    u = pi[0][x0]
    return u

def b_prob_one(N : int, x0 : int) -> float:
    model = InventoryDPModelB(N=N, prob_empty=True)
    J, pi = DP_stochastic(model)
    pr_empty = -J[0][x0]
    return pr_empty

