from irlc.ex04.model_pendulum import ContiniousPendulumModel
from irlc.ex04.continuous_time_discretized_model import DiscretizedModel
from irlc.exam.exam2023spring.dlqr import LQR
import numpy as np

def getAB(a : float):
    return np.asarray([[1,a], [0, 1]]), np.asarray([0, 1])[:,np.newaxis],  np.asarray([1, 0])

def a_LQR_solve(a : float, x0 : np.ndarray) -> float:
    A,B,d = getAB(a)
    Q = np.eye(2)
    R = np.eye(1)
    N = 100
    (L, l), _ = LQR(A=[A]*N, B=[B]*N, d=[d] * N, Q=[Q]*N, R=[R]*N)
    u = float( L[0] @ x0 + l[0])
    return u

def b_linearize(theta : float):
    model = ContiniousPendulumModel()
    dmodel = DiscretizedModel(model=model, dt=0.5)
    xbar = np.asarray([theta, 0])
    ubar = np.asarray([0])
    xp, A, B = dmodel.f(xbar, ubar, k=0, compute_jacobian=True)
    d = xp - A @ xbar - B @ ubar
    return A, B, d


def c_get_optimal_linear_policy(x0 : np.ndarray) -> float:
    x0 = np.asarray(x0)
    # xstar = np.asarray([np.pi/2, 0])
    Q = np.eye(2)
    R = np.eye(1)
    # q = -Q @ xstar
    # q0 = 0.5  * q@Q @q
    A, B, d = b_linearize(theta=0)
    N = 100
    (L, l), _ = LQR([A] * N, [B]*N, [d]*N, Q=[Q]*N, R=[R]*N)
    u = float(L[0] @ x0 + l[0])
    return u

